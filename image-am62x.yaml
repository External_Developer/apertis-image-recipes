{{ $architecture := or .architecture "arm64" }}
{{ $type := or .type "fixedfunction" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "v2023" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) }}
{{ $image := or .image (printf "apertis-%s-%s-%s" $suite  $type $architecture) }}

{{ $cmdline := or .cmdline "rootwait rw quiet splash plymouth.ignore-serial-consoles fsck.mode=auto fsck.repair=yes" }}

{{ $demopack := or .demopack "disabled" }}
{{ if eq $type "fixedfunction" }}
{{ $demopack := "disabled" }}
{{ end }}

{{- $package_updates_folder := or .package_updates_folder "" -}}

{{- $unpack := or .unpack "true" }}

architecture: {{ $architecture }}

actions:
{{- if eq $unpack "true" }}
  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}.tar.gz
{{- end }}

  - action: overlay
    description: Set the default bootcounter
    source: overlays/default-uboot-bootcount

  - action: overlay
    description: "Enable USB automount"
    source: overlays/usb-automount-rules

  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "fixedfunction" }}
    imagesize: 4G
{{ else }}
    imagesize: 15G
{{ end }}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
      - mountpoint: /boot
        partition: boot
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: boot
        fs: ext2
        start: 16M
        end: 256M
        flags: [ boot ]
      - name: system
        fs: ext4
        start: 256M
{{ if eq $type "fixedfunction" }}
        end: 3000M
{{ else }}
        end: 6000M
{{ end }}
      - name: general_storage
        fs: ext4
{{ if eq $type "fixedfunction" }}
        start: 3000M
{{ else }}
        start: 6000M
{{ end }}
        end: 100%

  - action: filesystem-deploy
    setup-kernel-cmdline: true
    append-kernel-cmdline: {{ $cmdline }}
    description: Deploying ospack onto image

  - action: run
    description: Add development apt sources
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} development

  # on arm64 the initramfs post-install does not call zz-u-boot-menu from
  # u-boot-menupackage when it is installed at the same time as the kernel
  # work around it by installing the the boot configuration tools first
  # see https://phabricator.apertis.org/T6325
  - action: apt
    description: Boot configuration packages
    packages:
      - initramfs-tools
      - u-boot-menu

{{- if ne $architecture "amd64" }}
  - action: overlay
    source: overlays/initramfs-modules-{{$architecture}}
{{ end }}

  - action: apt
    description: Kernel and system packages
    packages:
      - kmod
      - linux-base
{{ if eq $architecture "armhf" }}
      - linux-image-armmp
{{ else }}
      - linux-image-{{$architecture}}
{{ end }}

  - action: run
    description: Update packages
    chroot: true
    command: sh -c "sudo apt update"

  - action: apt
    description: Bootloader packages
    packages:
      - arm-trusted-firmware
      - device-tree-compiler
      - firmware-ti-am62xx
      - k3-image-gen
      - k3-image-gen-am62x
      - optee-os
      - u-boot-sitara
      - u-boot-tools

  # Magic numbers come from k3-image-gen "soc/am62x/Makefile"
  - action: run
    description: Assemble and sign R5 boot firmware
    chroot: false
    command:  ${ROOTDIR}/usr/lib/k3-image-gen/scripts/gen_x509_combined_cert.sh -b ${ROOTDIR}/usr/lib/u-boot/am62x_evm_r5/u-boot-spl.bin -l 0x43c00000 -s ${ROOTDIR}/lib/firmware/ti-sysfw/ti-fs-firmware-am62x-gp.bin -m 0x40000 -c "" -d ${ROOTDIR}/usr/lib/k3-image-gen/out/soc/am62x/evm/combined-tifs-cfg.bin -n 0x67000 -t ${ROOTDIR}/usr/lib/k3-image-gen/out/soc/am62x/evm/combined-dm-cfg.bin -y 0x43c3c800 -k ${ROOTDIR}/usr/lib/k3-image-gen/ti-degenerate-key.pem -r 1 -o ${ROOTDIR}/usr/lib/k3-image-gen/tiboot3-am62x-gp-evm.bin

  - action: run
    description: Assemble A53 boot firmware
    chroot: true
    script: scripts/assemble-am625-firmware.sh

  - action: run
    description: Move A53 boot firmware
    chroot: true
    command: sh -c "mv tispl.bin /usr/lib/u-boot/am62x_evm_a53/"

  - action: raw
    description: Install R5 SPL firmware
    origin: filesystem
    source: /usr/lib/k3-image-gen/tiboot3-am62x-gp-evm.bin
    offset: {{ sector 8192 }}

  - action: raw
    description: Install A53 SPL firmware
    origin: filesystem
    source: /usr/lib/u-boot/am62x_evm_a53/tispl.bin
    offset: {{ sector 1024 }}

  - action: raw
    description: Install A53 U-Boot firmware
    origin: filesystem
    source: /usr/lib/u-boot/am62x_evm_a53/u-boot.img
    offset: {{ sector 5120 }}

  - action: recipe
    description: Test on Merge Request
    recipe: snippet-merge-request-test.yaml
    variables:
      package_updates_folder: {{ $package_updates_folder }}

  - action: run
    description: Switch to live APT repos
    chroot: true
    script: scripts/switch-apt-to-live.sh -r {{ $suite }}

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $image }}.img.pkglist.gz"

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.sh

  # the clearing of machine-id can't be done before this point since
  # systemd-boot requires the machine-id to be set for reasons related to
  # dual-boot scenarios:
  # * to avoid conflicts when creating entries, see the `90-loaderentry` kernel
  #   install trigger
  # * to set the entries for the currently booted installation as default in
  #   the loader.conf generated by `bootctl install`
  #
  # in our image this is not useful, as the actual machine-id is supposed to be
  # uniquely generated on the first boot. however the impact is negligible, as
  # things still work albeit the code used to potentially disambiguate entries
  # doesn't match a real machine-id
  - action: run
    chroot: false
    description: "Empty /etc/machine-id so it's regenerated on first boot with an unique value"
    command: truncate -s0 "${ROOTDIR}/etc/machine-id"

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
  {{ if ne $demopack "disabled" }}
  # Use wget to get some insight about https://phabricator.collabora.com/T11930
  # TODO: Revert to a download action once the cause is found
  - action: run
    description: Download multimedia demo pack
    chroot: false
    command: wget --debug {{ $demopack }} -O "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  - action: unpack
    description: Unpack multimedia demo pack
    compression: gz
    file: multimedia-demo.tar.gz

  - action: run
    description: Clean up multimedia demo pack tarball
    chroot: false
    command: rm "${ARTIFACTDIR}/multimedia-demo.tar.gz"
  {{ end }}

  - action: run
    description: Generate BOM file
    chroot: false
    script: scripts/generate_bom.py -C -d "${ROOTDIR}/usr/share/doc" -s "${ROOTDIR}/var/lib/dpkg/status" -v 2 > ${ARTIFACTDIR}/{{ $image }}.img.licenses

  - action: run
    description: Delete /usr/share/doc
    chroot: false
    command: rm -rf "${ROOTDIR}"/usr/share/doc/*

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.img.filelist.gz"

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create "${ARTIFACTDIR}/{{ $image }}.img" > "${ARTIFACTDIR}/{{ $image }}.img.bmap"

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f "${ARTIFACTDIR}/{{ $image }}.img"

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum "${ARTIFACTDIR}/{{ $image }}.img.gz" > "${ARTIFACTDIR}/{{ $image }}.img.gz.sha256"
