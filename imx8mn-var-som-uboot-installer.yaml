{{ $architecture := or .architecture "arm64" }}
{{ $target := or .target "imx8mn_var_som" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "v2024dev2" }}
{{ $image := or .image (printf "uboot-%s-installer-%s" $suite $target) }}
{{- $osname := or .osname "apertis" -}}

architecture: {{ $architecture }}

actions:
  - action: image-partition
    imagename: {{ $image }}.img
    imagesize: 300M
    partitiontype: msdos

    mountpoints:
      - mountpoint: /
        partition: data
        options: [ ro ]

    partitions:
      - name: data
        fs: ext2
        start: 4096s
        end: 100%
        flags: [ boot ]

  - action: overlay
    source: overlays/installer_{{ $target }}

  - action: filesystem-deploy
    description: Deploying filesystem onto image

  - action: run
    description: Create chdist directory
    chroot: false
    command: mkdir "./chdist"

  - action: run
    description: Setup chdist
    chroot: false
    command: chdist -d "./chdist" create {{ $osname }}-{{ $suite }} {{ $mirror }} {{ $suite }} target non-free

  - action: run
    description: Add Apertis gpg key to chdist
    chroot: false
    command: cp "${RECIPEDIR}/keyring/{{ $osname }}-archive-keyring.gpg" "./chdist/{{ $osname }}-{{ $suite }}/etc/apt/trusted.gpg.d"

  - action: run
    description: Update repo (chdist)
    chroot: false
    command: chdist -d "./chdist" -a {{ $architecture }} apt {{ $osname }}-{{ $suite }} update

  - action: run
    description: Download U-Boot (chdist)
    chroot: false
    command: chdist -d "./chdist" -a {{ $architecture }} apt {{ $osname }}-{{ $suite }} download u-boot-imx

  - action: run
    description: Unpack U-Boot
    chroot: false
    command: dpkg -x u-boot-imx_*.deb "${ROOTDIR}/deb-binaries"

  - action: run
    description: Download imx-firmware-ddr4 (chdist)
    chroot: false
    command: chdist -d "./chdist" -a {{ $architecture }} apt {{ $osname }}-{{ $suite }} download imx-firmware-ddr4

  - action: run
    description: Unpack imx-firmware-ddr4
    chroot: false
    command: dpkg -x imx-firmware-ddr4_*.deb "${ROOTDIR}/deb-binaries"

  - action: run
    description: Wrapper installation script
    chroot: false
    command: mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "U-Boot eMMC Installation Script" -d "${ROOTDIR}/boot.cmd" "${ROOTDIR}/boot.scr"

  - action: run
    description: Build U-Boot image
    chroot: false
    script: scripts/imx8mn-uboot-image.sh "{{ $target }}" "${ROOTDIR}/deb-binaries/usr/lib/u-boot/{{ $target }}/flash.bin"

  - action: raw
    description: Install U-Boot
    origin: filesystem
    source: /deb-binaries/usr/lib/u-boot/{{ $target }}/flash.bin
    offset: {{ sector 64 }}

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create "${ARTIFACTDIR}/{{ $image }}.img" > "${ARTIFACTDIR}/{{ $image }}.img.bmap"

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f "${ARTIFACTDIR}/{{ $image }}.img"

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum "${ARTIFACTDIR}/{{ $image }}.img.gz" > "${ARTIFACTDIR}/{{ $image }}.img.gz.sha256"
