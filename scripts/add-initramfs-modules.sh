#!/bin/sh

set -e

# Add extra modules to initramfs
echo "I: Adding netprio_cgroup and cls_cgroup modules to initramfs"
echo netprio_cgroup >> /etc/initramfs-tools/modules
echo cls_cgroup >> /etc/initramfs-tools/modules
update-initramfs -u
