#!/bin/sh

set -e

if [ "$#" -ne 1 ] ; then
    echo Usage install_package_updates.sh FOLDER
    exit 1
fi

FOLDER=$1

echo Installing package updates from $FOLDER

DEB_INSTALL=""
for DEB in `find $1 -name '*deb'`; do
    PKG_NAME=`dpkg-deb -f $DEB Package`
    if dpkg -s $PKG_NAME >/dev/null 2>&1; then
        echo Package $PKG_NAME is installed, upgrading it
        DEB_INSTALL="$DEB_INSTALL $DEB"
    fi
done

if [ "$DEB_INSTALL" != "" ]; then
    echo Install installing $DEB_INSTALL
    apt install $DEB_INSTALL
fi

