#!/bin/sh


export ATF=/usr/lib/arm-trusted-firmware/k3/bl31.bin
export TEE=/usr/lib/optee/k3-am62x/tee-pager_v2.bin
export DM=/lib/firmware/ti-dm/am62xx/ipc_echo_testb_mcu1_0_release_strip.xer5f
export SPL=/usr/lib/u-boot/am62x_evm_a53/u-boot-spl-nodtb.bin

export ITS=u-boot-spl-k3.its

export LIST_OF_DTB="/usr/lib/u-boot/am62x_evm_a53/k3-am625-sk.dtb"

# Based on U-Boot's `tools/k3_fit_atf.sh`

cat > ${ITS} << __HEADER_EOF
/dts-v1/;

/ {
	description = "Configuration to load ATF and SPL";
	#address-cells = <1>;

	images {
		atf {
			description = "ARM Trusted Firmware";
			data = /incbin/("$ATF");
			type = "firmware";
			arch = "arm64";
			compression = "none";
			os = "arm-trusted-firmware";
			load = <0x9e780000>;
			entry = <0x9e780000>;
		};
		tee {
			description = "OPTEE";
			data = /incbin/("$TEE");
			type = "tee";
			arch = "arm64";
			compression = "none";
			os = "tee";
			load = <0x9e800000>;
			entry = <0x9e800000>;
		};
		dm {
			description = "DM binary";
			data = /incbin/("$DM");
			type = "firmware";
			arch = "arm32";
			compression = "none";
			os = "DM";
			load = <0x89000000>;
			entry = <0x89000000>;
		};
		spl {
			description = "SPL (64-bit)";
			data = /incbin/("$SPL");
			type = "standalone";
			os = "U-Boot";
			arch = "arm64";
			compression = "none";
			load = <0x80080000>;
			entry = <0x80080000>;
		};
__HEADER_EOF

for dtname in ${LIST_OF_DTB}
do
	cat >> ${ITS} << __FDT_IMAGE_EOF
		$(basename $dtname) {
			description = "$(basename $dtname .dtb)";
			data = /incbin/("$dtname");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
		};
__FDT_IMAGE_EOF
done

for dtname in ${LIST_OF_DTB}
do
cat >> ${ITS} << __CONF_HEADER_EOF
	};
	configurations {
		default = "$(basename $dtname)";

__CONF_HEADER_EOF
break
done

for dtname in ${LIST_OF_DTB}
do
	cat >> ${ITS} << __CONF_SECTION_EOF
		$(basename $dtname) {
			description = "$(basename $dtname .dtb)";
			firmware = "atf";
			loadables = "tee", "dm", "spl";
			fdt = "$(basename $dtname)";
		};
__CONF_SECTION_EOF
done

cat >> ${ITS} << __ITS_EOF
	};
};
__ITS_EOF

mkimage -f ${ITS} -E tispl.bin
