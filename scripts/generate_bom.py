#!/usr/bin/python3

from dataclasses import dataclass
from pathlib import Path
import sys
import os
from os.path import isfile, join, basename, dirname, normpath
from debian import copyright
import argparse
import itertools
import json
import gzip
import re
import typing

DEFAULT_BOM_DIR = '/usr/share/doc'
COPYRIGHT_REPORT = 'copyright_report'
COPYRIGHT_REPORT_GZ = COPYRIGHT_REPORT + '.gz'
DEFAULT_DPKG_STATUS = '/var/lib/dpkg/status'

VERBOSE_IMAGE = 0
VERBOSE_PACKAGE = 1
VERBOSE_BINARY = 2
VERBOSE_SOURCE = 3

NO_LICENSE_REPORT_FOUND = 'NoLicenseReportFound'
NO_COPYRIGHT_REPORT_FOUND = 'NoCopyrightReportFound'
NO_LICENSE_INFO_FOUND = 'NoLicenseInfoFound'
NO_COPYRIGHT_INFO_FOUND = 'NoCopyrightInfoFound'
AMBIGUOUS_LICENSE_INFO_FOUND = 'AmbiguousLicenseInfoFound'
AMBIGUOUS_COPYRIGHT_INFO_FOUND = 'AmbiguousCopyrightInfoFound'
NO_SOURCE_INFO_FOUND = 'NoSourceInfoFound'

# Levels of trickiness in dwarf2sources output
# This level is related to the debug information found in binaries, which
# depends on several things: build system, compiler, etc.
# This affects how easy is to get the path of the source file in the
# package in a reliable way.
# Each level computes in process_unit() following these high level ideas:
# TRICKINESS_LOW: Paths in DWARF information matches the source tree except
# by a prefix common for all the entries
# TRICKINESS_MEDIUM: Same as TRICKINESS_LOW but units have different comp_dir
# TRICKINESS_HIGH: Paths in DWARF information does not seem to match the
# source tree or there is no much information about source tree
# TRICKINESS_VERY_HIGH: Same as TRICKINESS_HIGH but also prefix does not much
# or there are suspicious strings in the file path

TRICKINESS_LOW = 0
TRICKINESS_MEDIUM = 1
TRICKINESS_HIGH = 2
TRICKINESS_VERY_HIGH = 4

# Threshold from which a report is considered ambiguous
# and a full copyright is needed to avoid ambiguity
AMBIGUITY_THRESHOLD = TRICKINESS_VERY_HIGH

COPYRIGHT_LENGTH = 500
COPYRIGHT_CONTAINS_NONASCII_CHARS = 'CopyrightContainsNonAsciiCharacters'

# Regexp to match bin2sources filename:
# If matching, group 1 will contain package name while group 3 contains the
# package arch (the "_$arch" variant was not included in previous
# versions of the filename, so let's keep compatibility with old versions by
# making it optional)
re_bin2sources = re.compile(
    '(?P<package>.+)_bin2sources(?:_(?P<arch>[a-zA-Z0-9]+))?\\.json(\\.gz)?$'
)


def get_base_package_name(dirpath):
    return basename(dirpath)


def open_potentially_gzipped(path, *args, **kw):
    if str(path).endswith('.gz'):
        return gzip.open(path, *args, **kw)
    else:
        return open(path, *args, **kw)


def parse_copyright(copyright):
    """Parse copyrights to ensure each item contains only one copyright and
       not a string of several copyrights

    >>> copyright = ["2012-2014, Marc Hoersken <info@marc-hoersken.de>\\n 2012, Mark Salisbury <mark.salisbury@hp.com>\\n 2012-2015, Daniel Stenberg <daniel@haxx.se>",
    ...              "2009, 2011, Markus Moeller, <markus_moeller@compuserve.com>\\n 2012-2015, Daniel Stenberg, <daniel@haxx.se>"]

    >>> parse_copyright(copyright)
    ['2009, 2011, Markus Moeller, <markus_moeller@compuserve.com>', '2012, Mark Salisbury <mark.salisbury@hp.com>', '2012-2014, Marc Hoersken <info@marc-hoersken.de>', '2012-2015, Daniel Stenberg <daniel@haxx.se>', '2012-2015, Daniel Stenberg, <daniel@haxx.se>']
    """
    list_copyright = [i.splitlines() for i in copyright]
    list_copyright = {i.strip() for i in itertools.chain.from_iterable(list_copyright)}
    return sorted(list_copyright)


@dataclass
class PackageCopyrightFiles:
    primary_copyright: typing.Optional[copyright.Copyright]
    external_source_copyrights: typing.Dict[str, copyright.Copyright]
    binaries: typing.Dict[str, typing.Any]
    metadata: typing.Dict[str, typing.Any]

    @staticmethod
    def load(package_name, *, primary_copyright_path, external_copyrights_path,
             bin2sources_path, metadata_path):
        primary_copyright = None
        if primary_copyright_path is not None:
            with open_potentially_gzipped(primary_copyright_path) as f_copyright:
                try:
                    primary_copyright = copyright.Copyright(f_copyright, strict=False)
                except Exception as ex:
                    print(f'WARNING package {package_name} tries to use invalid copyright file'
                          f' {primary_copyright_path}: {ex}', file=sys.stderr)

        external_source_copyrights = {}
        external_copyrights_path = Path(external_copyrights_path)

        for report in itertools.chain(
                external_copyrights_path.glob(f'*.{COPYRIGHT_REPORT}'),
                external_copyrights_path.glob(f'*.{COPYRIGHT_REPORT_GZ}')):
            # We can't just use os.path.splitext, because .gz paths need
            # *two* extensions. Instead, we can take advantage that the .gz
            # extension still starts with the non-compressed report
            # extension, and just strip off everything after the non-gz
            # extension's position.
            external_package = report.name[:report.name.rindex(f'.{COPYRIGHT_REPORT}')]
            with open_potentially_gzipped(report) as f_copyright:
                try:
                    external_source_copyrights[external_package] = copyright.Copyright(f_copyright, strict=False)
                except Exception as ex:
                    print(f'WARNING package {package_name} dependency {external_package} tries to use'
                          f' invalid copyright file {report}: {ex}', file=sys.stderr)

        with open_potentially_gzipped(bin2sources_path) as f_binaries:
            binaries = json.load(f_binaries)

        metadata = {}
        metadata_path = Path(metadata_path)
        if metadata_path.exists():
            with open_potentially_gzipped(metadata_path) as f_metadata:
                try:
                    metadata = json.load(f_metadata)
                except Exception as ex:
                    print(f'WARNING package {package_name} tries to use invalid metadata file'
                          f' {metadata_path}: {ex}', file=sys.stderr)
                    metadata = {}

        return PackageCopyrightFiles(primary_copyright, external_source_copyrights, binaries, metadata)

    @property
    def external_source_index(self):
        return self.metadata.get('external_sources_to_packages', {})

    def find_copyright_for_path(self, path):
        if (path.startswith('/')
                and (package := self.external_source_index.get(path))
                and (external_copyright_info := self.external_source_copyrights.get(package))):
            return external_copyright_info

        return self.primary_copyright


class PathDepth():
    def __init__(self):
        self.path_depth = {}

    def get_path_depths(self):
        return self.path_depth

    def insert_path(self, p):
        depth = p.count('/')
        if depth in self.path_depth:
            paths = self.path_depth[depth]
            paths.add(dirname(p))
        else:
            self.path_depth[depth] = {dirname(p)}

    def get_depths(self):
        return sorted(self.path_depth.keys())

    def get_paths(self, pd):
        return self.path_depth[pd]


class Trickiness():
    def __init__(self, trickiness = TRICKINESS_LOW):
        self.trickiness = trickiness

    def get(self):
        return self.trickiness

    def update(self, new_trickiness):
        if new_trickiness > self.trickiness:
            self.trickiness = new_trickiness


class BomGenerator():
    def __init__(self, bom_dir, dpkg_status, verbose, pretty, comments, copyright, copyright_limit, error_on_tricky):
        self.bom_dir = bom_dir
        self.dpkg_status = dpkg_status
        self.verbose = verbose
        self.pretty = pretty
        self.comments = comments
        self.copyright = copyright
        self.copyright_limit = int(copyright_limit)
        self.error_on_tricky = error_on_tricky

    def get_license_str(self, paragraph):
        license = paragraph.license[0]
        if self.comments:
            license += ' ' + str(paragraph.comment)

        return license

    def get_copyright_str(self, paragraph):
        copyright = paragraph.copyright

        if len(copyright) > self.copyright_limit:
            return copyright[:self.copyright_limit]

        if not copyright.isascii():
            return COPYRIGHT_CONTAINS_NONASCII_CHARS

        return copyright

    def check_copyright_full(self, copyright_info):
        for p in copyright_info.all_files_paragraphs():
            for f in p.files:
                if "/*" in f:
                    return False
        return True

    def find_files_paragraphs_full(self, copyright_info, f_src):
        for p in copyright_info.all_files_paragraphs():
            for f in p.files:
                if f.endswith(f_src):
                    return p

        # If we didn't find a match with the full path
        # check using just the file name
        # but also check that the same file name does not appear twice
        paragraphs = []
        for p in copyright_info.all_files_paragraphs():
            for f in p.files:
                if f.endswith(basename(f_src)):
                    paragraphs.append(p)

        return paragraphs

    # Get the copyright for file by searching the exact file path
    def get_license_copyright(self, copyright_info, copyright_full, trickiness, f_src):
        if trickiness >= AMBIGUITY_THRESHOLD  and not copyright_full and self.error_on_tricky:
            return { 'license': AMBIGUOUS_LICENSE_INFO_FOUND, 'copyright': AMBIGUOUS_COPYRIGHT_INFO_FOUND}

        p = copyright_info.find_files_paragraph(f_src)
        if p:
            return { 'license': self.get_license_str(p),'copyright': self.get_copyright_str(p)}

        # If paragraph was not found in a copyright with full information
        # most probably the file path from dwarf2sources is wrong
        # try to guess the right file
        if copyright_full:
            p = self.find_files_paragraphs_full(copyright_info, f_src)
            if p:
                if len(p) == 1:
                    return { 'license': self.get_license_str(p[0]),'copyright': self.get_copyright_str(p[0])}
                else:
                    return { 'license': AMBIGUOUS_LICENSE_INFO_FOUND, 'copyright': AMBIGUOUS_COPYRIGHT_INFO_FOUND}

        return { 'license': NO_LICENSE_INFO_FOUND, 'copyright': NO_COPYRIGHT_INFO_FOUND}

    def get_copyright_file(self, filenames):
        if COPYRIGHT_REPORT in filenames:
            return COPYRIGHT_REPORT
        if COPYRIGHT_REPORT_GZ in filenames:
            return COPYRIGHT_REPORT_GZ

        return None

    def preprocess_unit(self, units):
        # Avoid processing no real entries that can be produced by dwarf2sources
        # when using lto optimization
        units[:] = [x for x in units if x['comp_name'] != '<artificial>']

        # Compute file path
        for un in units:
            un['file_path'] = normpath(join(un['comp_dir'], un['comp_name']))

    # Used to build a dictionary with all the path depths in the
    # copyright file and the correspondant paths
    # This information will be used to try to guess the path_prefix
    def get_copyright_path_depth(self, copyright_info):
        path_depth = PathDepth()

        for p in copyright_info.all_files_paragraphs():
            for f in p.files:
                if f.startswith('debian'):
                    continue

                path_depth.insert_path(f)

        return path_depth

    # Used to get the path_prefix which should be removed before try to match
    # with the information in the copyright file
    # By doing this the chance of picking a default license is reduced
    def get_path_prefix_and_depth(self, copyright_info, units):
        path_depth = self.get_copyright_path_depth(copyright_info)
        depths = path_depth.get_depths()

        for pd_key in reversed(depths):
            for un in units:
                for p in path_depth.get_paths(pd_key):
                    if un['file_path'].startswith(p + '/'):
                        return ('', pd_key)
                    else:
                        path_prefix_len = un['file_path'].find('/' + p + '/')
                    if path_prefix_len != -1:
                        return (un['file_path'][:path_prefix_len + 1], pd_key)

        return ('', -1)

    # This function does a few things
    # 1 determine the trickiness of the unit based on the path information
    # 2 normalize path if a prefix can be removed
    # 3 updates the trickiness after computed the path
    def process_unit(self, units, path_prefix, depth):
        default_f_dir = ''
        default_up_folder = -1
        trickiness = Trickiness(TRICKINESS_LOW)
        for un in units:
            if default_f_dir == '':
                default_f_dir = un['comp_dir']
            if default_up_folder == -1:
                default_up_folder = un['comp_name'].rfind('../')

            if un['comp_dir'] != default_f_dir:
                trickiness.update(TRICKINESS_MEDIUM)

            up_folder = un['comp_name'].rfind('../')
            if up_folder != default_up_folder:
                trickiness.update(TRICKINESS_MEDIUM)

            if path_prefix != '':
                # If prefix matches all good
                if un['file_path'].startswith(path_prefix):
                    un['file_path'] = un['file_path'].removeprefix(path_prefix)
                else:
                    trickiness.update(TRICKINESS_VERY_HIGH)
            else:
                if depth > 0:
                    trickiness.update(TRICKINESS_MEDIUM)
                else:
                    trickiness.update(TRICKINESS_VERY_HIGH)

            TRICKY_STRINGS = ['debian', 'build']
            if any(ts in dirname(un['file_path']).lower() for ts in TRICKY_STRINGS):
                trickiness.update(TRICKINESS_VERY_HIGH)

        return trickiness.get()

    def scan_rust_unit(self, f_dir, f_src, copyright_info, out_licenses, out_copyright):
        # Rust files only have one compilation unit for the entire
        # crate, so as a workaround, we can just match against all
        # licenses under the parent folder of the CU's entry point. *In
        # general*, this should find everything. The main cases where it
        # wouldn't work are:
        # - Someone is explicitly loading modules from a different
        #   directory.
        # - A library and an executable crate are both in the same
        #   folder. In the latter case, we just overestimate the
        # license, which is preferred to underestimating it! The former
        # case is exceptionally rare.

        f_src_parent = os.path.dirname(os.path.relpath(f_src, f_dir))
        # Make sure there's a trailing slash, so 'xy/' doesn't match
        # 'x/*'.
        if not f_src_parent.endswith('/'):
            f_src_parent += '/'
            pass

        for paragraph in copyright_info.all_files_paragraphs():
            if paragraph.matches(f_src_parent):
                out_licenses.add(self.get_license_str(paragraph))

                if self.copyright:
                    out_copyright.add(self.get_copyright_str(paragraph))

        if not out_licenses:
            out_licenses.add(NO_LICENSE_INFO_FOUND)
        if self.copyright and not out_copyright:
            out_copyright.add(NO_COPYRIGHT_INFO_FOUND)

    def scan_regular_unit(self, unit_file_path, copyright_info, copyright_full, trickiness, out_licenses, out_copyright):
        license_copyright = self.get_license_copyright(copyright_info, copyright_full, trickiness, unit_file_path)
        out_licenses.add(license_copyright['license'])

        if self.copyright:
            out_copyright.add(license_copyright['copyright'])

    def scan_units(self, package_name, binary_name, package_copyright_files, units):
        binary_licenses = set()
        binary_copyright = set()
        sources = []

        if not package_copyright_files.primary_copyright:
            if self.copyright:
                return {'binary_name': binary_name, 'binary_licenses': [NO_LICENSE_REPORT_FOUND],
                        'binary_copyright': [NO_COPYRIGHT_REPORT_FOUND], 'sources': sources}
            else:
                return {'binary_name': binary_name, 'binary_licenses': [NO_LICENSE_REPORT_FOUND], 'sources': sources}

        copyright_full = self.check_copyright_full(package_copyright_files.primary_copyright)
        self.preprocess_unit(units)
        (path_prefix, depth) = self.get_path_prefix_and_depth(package_copyright_files.primary_copyright, units)
        trickiness = self.process_unit(units, path_prefix, depth)

        if trickiness >= AMBIGUITY_THRESHOLD  and not copyright_full:
            print(f'WARNING: package_name {package_name} mapping is trickiest than threshold, use full copyright', file=sys.stderr)

        for un in units:
            file_copyright_info = package_copyright_files.find_copyright_for_path(un['comp_name'])

            file_licenses = set()
            file_copyright = set()

            # Rust packages have a particular style of compilation unit paths,
            # so handle those separately, ensuring we get the proper source file
            # name to match against the copyright info.
            if un['file_path'].startswith('/usr/share/cargo/registry'):
                self.scan_rust_unit(un['comp_dir'], un['comp_name'], file_copyright_info, file_licenses, file_copyright)
            else:
                un['file_path'] = un['file_path'].removeprefix(path_prefix)
                self.scan_regular_unit(un['file_path'], file_copyright_info, copyright_full, trickiness, file_licenses, file_copyright)

            if self.verbose > VERBOSE_BINARY:
                for license in file_licenses:
                    sources.append({'source': un['file_path'], 'license': str(license)})

            binary_licenses |= file_licenses
            binary_copyright |= file_copyright

        if self.copyright:
            list_binary_copyright = parse_copyright(binary_copyright)
            return {'binary_name': binary_name, 'binary_licenses': list(binary_licenses),
                    'binary_copyright': list_binary_copyright, 'sources': sources}
        else:
            return {'binary_name': binary_name, 'binary_licenses': list(binary_licenses), 'sources': sources}

    def check_binaries_copyright(self, package_name, package_copyright_files):
        package_licenses = set()
        package_copyright = set()
        binaries = []
        for (binary_name, binary_info) in package_copyright_files.binaries.items():
            binary = self.scan_units(package_name, binary_name, package_copyright_files, binary_info['units'])
            if self.verbose > VERBOSE_PACKAGE:
                binaries.append(binary)

            package_licenses.update(binary['binary_licenses'])
            if self.copyright:
                package_copyright.update(binary['binary_copyright'])

        if self.copyright:
            list_package_copyright = parse_copyright(package_copyright)
            return {'package_name': package_name, 'package_licenses': list(package_licenses),
                    'package_copyright': list_package_copyright, 'binaries': binaries}
        else:
            return {'package_name': package_name, 'package_licenses': list(package_licenses), 'binaries': binaries}

    def get_installed_packages(self):
        installed_packages = set()
        with open(self.dpkg_status) as dpkg_status:
            for l in dpkg_status.readlines():
                if l.startswith('Package:'): installed_packages.add(l.split(' ')[1].strip())

        return installed_packages

    def check_packages_copyright(self):
        image_licenses = set()
        packages = []
        processed_packages = set()
        installed_packages = self.get_installed_packages()

        for d in os.listdir(self.bom_dir):
            dirpath = join(self.bom_dir, d)
            if not os.path.isdir(dirpath):
                continue
            filenames = os.listdir(dirpath)
            if len(filenames) == 0:
                continue
            base_package_name = get_base_package_name(dirpath)
            fn_copyright = self.get_copyright_file(filenames)
            if fn_copyright:
                fn_copyright = join(dirpath, fn_copyright)
            else:
                print(f'WARNING: folder {base_package_name} does not contain copyright report', file=sys.stderr)

            for f in filenames:
                if not isfile(join(dirpath, f)) or not (m := re_bin2sources.match(f)):
                    continue

                package_name = m.group('package')
                arch = m.group('arch')

                f = join(dirpath, f)
                fn_external = join(dirpath, 'external_copyrights')
                fn_metadata = join(dirpath, f'{package_name}_metadata_{arch}.json')

                package_copyright_files = PackageCopyrightFiles.load(package_name,
                                                                     primary_copyright_path=fn_copyright,
                                                                     external_copyrights_path=fn_external,
                                                                     bin2sources_path=f,
                                                                     metadata_path=fn_metadata)
                package = self.check_binaries_copyright(package_name, package_copyright_files)
                processed_packages.add(package_name)
                if self.verbose > VERBOSE_IMAGE:
                    packages.append(package)

                image_licenses.update(package['package_licenses'])

        missing_packages = installed_packages.difference(processed_packages)
        missing_packages = list(missing_packages)
        missing_packages.sort()
        if len(missing_packages):
            print(f'WARNING: there are packages without license information', file=sys.stderr)
            if self.verbose > VERBOSE_IMAGE:
                for p in missing_packages:
                    packages.append({'package_name': p, 'package_licenses': [NO_SOURCE_INFO_FOUND]})

            image_licenses.update([NO_SOURCE_INFO_FOUND])

        image = {'image_licenses': list(image_licenses), 'packages': packages}

        kwargs = {}
        if self.pretty:
            kwargs["indent"] = 4
        print(json.dumps(image, **kwargs))


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--comments", action='store_true', help="include license comments")
    parser.add_argument("-C", "--copyright", action='store_true', help="include copyright information")
    parser.add_argument("-l", "--copyright_limit", default=COPYRIGHT_LENGTH,
                        help="limit maximum number of characters in copyright information")
    parser.add_argument("-d", "--dir", default=DEFAULT_BOM_DIR, help="directory to search for information")
    parser.add_argument("-p", "--pretty", action='store_true', help="indent the JSON output")
    parser.add_argument("-s", "--dpkg-status", default=DEFAULT_DPKG_STATUS, help="dpkg status file")
    parser.add_argument("-t", "--error-on-tricky", action='store_true', help="set error on tricky units")
    parser.add_argument("-v", "--verbose", type=int, default=VERBOSE_IMAGE,
                        help="verbose use in output 0: image, 1: package, 2: binary, 3: source")

    args = parser.parse_args()

    bom_generator = BomGenerator(args.dir, args.dpkg_status, args.verbose, args.pretty, args.comments,
                                 args.copyright, args.copyright_limit, args.error_on_tricky)

    bom_generator.check_packages_copyright()


if __name__ == '__main__':
    main(sys.argv[1:])
