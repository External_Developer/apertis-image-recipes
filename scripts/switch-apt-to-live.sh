#!/bin/sh

set -eu

SRCLIST=/etc/apt/sources.list
RELEASE=

opts=$(getopt -o "r:" -l "release:" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        -r|--release) RELEASE="$2"; shift 2;;
        --) shift; break;;
        *) ;;
    esac
done

if [ -z "$RELEASE" ]
then
    echo "Please provide release." >&2
    exit 1
fi

sed -i -r 's/(deb|deb-src) ([^ ]+) ('"$RELEASE"'(-updates|-security)?)\/snapshots\/[^ ]+ +(.*)$/#\0\n\1 \2 \3 \5/' "$SRCLIST"

# Do not update lists so that images are reproducible
