#!/usr/bin/python3

import sys
import argparse
import json
import requests

ARCH_MAP = {
    'amd64': 'x86_64',
    'armhf': 'armv7hl',
    'arm64': 'aarch64'
}

def get_latest_job_id(url_base, mr_iid, arch, token):
    headers = {'PRIVATE-TOKEN': token}

    r = requests.get(url_base, headers=headers)

    if r.status_code != 200:
        print(f'Error retrieving job information, status {r.status_code}', file=sys.stderr)
        sys.exit(1)

    jobs = r.json()

    ref = 'refs/merge-requests/' + str(mr_iid) + '/head'
    if arch not in ARCH_MAP.keys():
        print(f'Invalid arch {arch}', file=sys.stderr)
        sys.exit(1)

    name = 'obs-default-' + ARCH_MAP[arch]
    for j in jobs:
        if j['ref'] == ref and j['name'] == name:
            return j['id']

    return None

def get_url(url_base, job_id):
    return url_base + str(job_id) + '/artifacts'

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab_server', default='gitlab.apertis.org', help='URL base')
    parser.add_argument('--token', help='token')
    parser.add_argument('--mr_iid', help='Merge Request IID')
    parser.add_argument('--arch', help='architecture')
    parser.add_argument('project_path', help='Project path')

    args = parser.parse_args()

    project_path = requests.utils.quote(args.project_path, safe='')
    url_base = 'https://' + args.gitlab_server + '/api/v4/projects/' + project_path + '/jobs/'

    job_id = get_latest_job_id(url_base, args.mr_iid, args.arch, args.token)

    if not job_id:
        print('Unable to find job', file=sys.stderr)
        sys.exit(1)

    print(get_url(url_base, job_id))

if __name__ == '__main__':
    main(sys.argv[1:])
