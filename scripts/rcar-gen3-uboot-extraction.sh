#! /bin/sh
set -eu

SUITE=${1:-v2021dev3}
MIRROR=${2:-https://repositories.apertis.org/apertis/}
OSNAME=${3:-apertis}

mkdir "${WORKSPACE}/chdist"

echo "Setup chdist"
chdist -d "${WORKSPACE}/chdist" create apertis-${SUITE} ${MIRROR} ${SUITE} target

echo "Add Apertis gpg key to chdist"
cp "${WORKSPACE}/keyring/${OSNAME}-archive-keyring.gpg" "${WORKSPACE}/chdist/apertis-${SUITE}/etc/apt/trusted.gpg.d"

echo "Update repo (chdist)"
chdist -d "${WORKSPACE}/chdist" -a arm64 apt apertis-${SUITE} update

echo "Download U-Boot (chdist)"
chdist -d "${WORKSPACE}/chdist" -a arm64 apt apertis-${SUITE} download u-boot-rcar-gen3

echo "Unpack U-Boot"
dpkg -x u-boot-rcar-gen3_*.deb "${WORKSPACE}/deb-binaries"

echo "Grab U-Boot"
mv "${WORKSPACE}/deb-binaries/usr/lib/u-boot" "./u-boot"

echo "Clean up"
rm -Rvf u-boot-rcar-gen3_*.deb "${WORKSPACE}/deb-binaries" "${WORKSPACE}/chdist"
