#!/bin/sh

set -e

SUITE=$1

usage() {
    echo "Usage: $0 VERSION"
    echo "    VERSION   Apertis version number"
}

if [ -z "${SUITE}" ]; then
    usage
    exit 1
fi

# Configure Apertis flatpak repository
flatpak remote-add apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo

# Install the Flatdeb example applications
flatpak install --noninteractive \
    org.apertis.hmi.totem//stable

# Configure Flathub repository
flatpak remote-add --no-sign-verify flathub https://flathub.org/repo/flathub.flatpakrepo
