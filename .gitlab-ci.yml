image: $DOCKER_IMAGE

######
# variables
#
# The main variables to customize the built artifacts

variables:
  osname: apertis
  release: "v2024dev2"
  upload_host: archive@images.apertis.org
  upload_host_ostree_port: '2222'
  upload_root_main: /srv/images/public
  upload_root_test: /srv/images/test/$CI_COMMIT_REF_SLUG
  image_url_prefix_main: https://images.apertis.org
  image_url_prefix_test: https://images.apertis.org/test/$CI_COMMIT_REF_SLUG
  ostree_path: ostree/repo/
  test_repo_url: https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/tests/apertis-test-cases.git
  demopack: https://images.apertis.org/media/multimedia-demo.tar.gz
  collection_id: org.apertis.os
  mirror: https://repositories.apertis.org/apertis/
  hawkbit_server_url: https://hawkbit.apertis.org
  qa_report_url: https://lavaphabbridge.apertis.org/
  check_bom: 1

.snippets:
  - &osname_branch_rule $CI_COMMIT_BRANCH =~ /^apertis\//

default:
  interruptible: true
  retry:
    max: 1
    when:
      # sometimes the jobs takes more time than expected due to slow network traffic or other unrelated issues
      # some other times the fakemachine startup gets stuck while mounting filesystems and drops into the emergency shell
      # https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/jobs/332907
      #   [FAILED] Failed to mount /etc/alternatives.
      #   See 'systemctl status etc-alternatives.mount' for details.
      #   [DEPEND] Dependency failed for Local File Systems.
      - job_execution_timeout
      # in some cases UML seems behave surprisingly, leading to crashes up in the stack, for instance in the ostree
      # locking from debos
      # https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/jobs/333401
      #   (process:1593): OSTree-CRITICAL **: 16:03:48.541: pop_repo_lock: assertion 'lock_table != NULL' failed
      #   (process:1593): GLib-CRITICAL **: 16:03:48.542: g_propagate_error: assertion 'src != NULL' failed
      #   ERROR:/usr/local/go/src/github.com/sjoerdsimons/ostree-go/pkg/glibobject/glibobject.go.h:6:_g_error_get_message: assertion failed: (error != NULL)
      #   SIGABRT: abort
      - script_failure

stages:
  - build-env
  - preparation
  - ospack build
  - ostree commit
  - firmware extraction
  - installer image build
  - apt image build
  - ostree image build
  - ostree bundle build
  - ostree rollbackbundle build
  - sysroot build
  - oslist build
  - artifacts upload
  - sysroot metadata upload
  - installer upload
  - firmware upload
  - ostree hawkbit upload
  - ostree push
  - generate tests
  - run tests

######
# Permissions fixup
#
# GitLab uses a too-lax umask when checking out the repositories, which leads
# to root-owned  world-writable files being put in the images.
#
# The image-builder container defaults to umask 022 which would only affects
# the w bit, so reset it for group and other users.
#
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/1736
before_script: &gitlab_permissions_fixup
  - chmod -R og-w .
  - chmod -R a-w overlays/sudo-fqdn

######
# templates
#
# The files included here contain the actual definitions of the jobs as templates
# that need to be instantiated with `extends` while setting the variables that
# control which ospack/hwpack to use.

include:
  - /.gitlab-ci/templates-ospack-build.yml
  - /.gitlab-ci/templates-ostree-commit.yml
  - /.gitlab-ci/templates-artifacts-build.yml
  - /.gitlab-ci/templates-upload.yml
  - /.gitlab-ci/templates-generate-tests.yml
  - /.gitlab-ci/templates-run-tests.yml
  - /.gitlab-ci/templates-rpi-oslist.yml

.lightweight:
  tags:
    - lightweight

######
# ospacks
#
# These variables are meant to be used when instantiating the templates
# included above to choose the desired ospack flavours

.fixedfunction:
  variables:
    type: fixedfunction

.hmi:
  variables:
    type: hmi
    debosarguments:
      -t demopack:${demopack}
      --scratchsize 10G

.basesdk:
  variables:
    type: basesdk
    check_bom: 0
    debosarguments:
      -t demopack:${demopack}
      -t devrootpack:../../armhf/devroot/ospack_${release}-armhf-devroot_${PIPELINE_VERSION}.tar.gz
      --scratchsize 20G

.sdk:
  variables:
    type: sdk
    check_bom: 0
    debosarguments:
      -t sampleappscheckout:enabled
      -t demopack:${demopack}
      -t devrootpack:../../armhf/devroot/ospack_${release}-armhf-devroot_${PIPELINE_VERSION}.tar.gz
      --scratchsize 20G

.devroot:
  variables:
    type: devroot

.sysroot:
  variables:
    type: sysroot

######
# hwpacks
#
# Another set of variables meant to be used when instantiating the templates
# included above, choosing the hwpack flavors

.amd64-uefi:
  variables:
    architecture: amd64
    board: uefi

.amd64-sdk:
  variables:
    architecture: amd64
    board: sdk

.armhf-uboot:
  variables:
    architecture: armhf
    board: uboot

.arm64-uboot:
  variables:
    architecture: arm64
    board: uboot

.arm64-rpi64:
  variables:
    architecture: arm64
    board: rpi64

.arm64-rk3399:
  variables:
    architecture: arm64
    board: rk3399

.arm64-am62x:
  variables:
    architecture: arm64
    board: am62x

######
# Boards flavours

.arm64-roc-pc-rk3399:
  variables:
    sbc: roc-pc-rk3399

.arm64-rock-pi-4-rk3399:
  variables:
    sbc: rock-pi-4-rk3399

.arm64-rockpro64-rk3399:
  variables:
    sbc: rockpro64-rk3399

######
# stage: preparation

debos-dry-run:
  stage: preparation
  extends:
    - .lightweight
  script:
    - 'for i in *.yaml ; do debos --print-recipe --dry-run "$i" ; done'

prepare-build-env:
  stage: build-env
  extends:
    - .lightweight
  image: debian:bullseye-slim
  before_script:
    - apt update && apt install -y --no-install-recommends
        ca-certificates
        curl
        jq
        skopeo
  script:
    # multiline variables have CRLF line endings, likely because
    # x-www-form-urlencoded and form-data mandate them, so let's get rid of the
    # extra `\r` and ensure there's a line terminator at the end of the file
    # (by appending nothing, but it works)
    - test -n "$BUILD_ENV_OVERRIDE" && sed -i 's/\r$// ; $a\'  "$BUILD_ENV_OVERRIDE" && . "$BUILD_ENV_OVERRIDE"
    - PIPELINE_VERSION=${PIPELINE_VERSION:-$(date +%Y%m%d.%H%M)}
    - DOCKER_IMAGE_BASE=$CI_REGISTRY/infrastructure/${osname}-docker-images/${release}-image-builder
    - DOCKER_IMAGE=${DOCKER_IMAGE:-$DOCKER_IMAGE_BASE@$(skopeo --creds "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" inspect docker://$DOCKER_IMAGE_BASE | jq -r .Digest)}
    - mkdir -p a/$PIPELINE_VERSION/meta
    - BUILD_ENV=a/$PIPELINE_VERSION/meta/build-env-$CI_PROJECT_NAME.txt
    - echo "PIPELINE_VERSION=$PIPELINE_VERSION" | tee -a $BUILD_ENV
    - echo "DOCKER_IMAGE=$DOCKER_IMAGE" | tee -a $BUILD_ENV
    - echo "RECIPES_COMMIT=$CI_COMMIT_SHA" | tee -a $BUILD_ENV
    - echo "RECIPES_URL=$CI_PROJECT_URL"  | tee -a $BUILD_ENV
    - echo "PIPELINE_URL=$CI_PIPELINE_URL" | tee -a $BUILD_ENV
    - APT_SNAPSHOT_LATEST_URL="${mirror%/}/dists/${release}/snapshots/latest.txt"
    - echo "Fetching $APT_SNAPSHOT_LATEST_URL"
    - APT_SNAPSHOT_LATEST=$(curl --fail --silent --show-error "$APT_SNAPSHOT_LATEST_URL")
    - APT_SNAPSHOT=${APT_SNAPSHOT:-$APT_SNAPSHOT_LATEST}
    - echo "APT_SNAPSHOT=$APT_SNAPSHOT" | tee -a $BUILD_ENV

    - echo "PACKAGE_UPDATES_PROJECT_PATH=$PACKAGE_UPDATES_PROJECT_PATH" | tee -a $BUILD_ENV
    - |
      if [ "$PACKAGE_UPDATES_PROJECT_PATH" != "" ] ; then
        UPLOAD_ARTIFACTS=1
        SUBMIT_LAVA_JOBS=1
      fi
    - echo "PACKAGE_UPDATES_MR_IID=$PACKAGE_UPDATES_MR_IID" | tee -a $BUILD_ENV
    - echo "PACKAGE_UPDATES_TESTS=$PACKAGE_UPDATES_TESTS" | tee -a $BUILD_ENV
    - |
      case "$CI_COMMIT_REF_NAME" in
        "$osname"/*)
          upload_root=$upload_root_main
          image_url_prefix=$image_url_prefix_main
          upload_artifacts=1
          submit_lava_jobs=1
          ;;
        *)
          upload_root=$upload_root_test
          image_url_prefix=$image_url_prefix_test
          upload_artifacts=0
          submit_lava_jobs=0
          ;;
      esac
    - echo "IMAGE_URL_PREFIX=${IMAGE_URL_PREFIX:-$image_url_prefix}" | tee -a $BUILD_ENV
    - echo "UPLOAD_ROOT=$upload_root" | tee -a $BUILD_ENV # cannot be overridden to avoid overwriting previous uploads
    - echo "UPLOAD_ARTIFACTS=${UPLOAD_ARTIFACTS:-$upload_artifacts}" | tee -a $BUILD_ENV
    - echo "SUBMIT_LAVA_JOBS=${SUBMIT_LAVA_JOBS:-$submit_lava_jobs}" | tee -a $BUILD_ENV
    - test -n "$BUILD_ENV_OVERRIDE" && diff --color=always -U 10 "$BUILD_ENV_OVERRIDE" "$BUILD_ENV" || true
  artifacts:
    expire_in: 1d
    paths:
      - a/*/meta/build-env-$CI_PROJECT_NAME.txt
    reports:
      dotenv: a/*/meta/build-env-$CI_PROJECT_NAME.txt

######
# stage: ospack build

ospack-build-amd64-fixedfunction:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .fixedfunction

ospack-build-armhf-fixedfunction:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .fixedfunction

ospack-build-arm64-fixedfunction:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .fixedfunction

ospack-build-amd64-hmi:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .hmi

ospack-build-armhf-hmi:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .hmi

ospack-build-arm64-hmi:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .hmi

ospack-build-amd64-basesdk:
  extends:
    - .ospack-build
    - .amd64-sdk
    - .basesdk

ospack-build-amd64-sdk:
  extends:
    - .ospack-build
    - .amd64-sdk
    - .sdk

ospack-build-amd64-devroot:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .devroot

ospack-build-armhf-devroot:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .devroot

ospack-build-arm64-devroot:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .devroot

ospack-build-amd64-sysroot:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .sysroot

ospack-build-armhf-sysroot:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .sysroot

ospack-build-arm64-sysroot:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .sysroot

######
# stage: ostree commit

ostree-commit-amd64-fixedfunction-uefi:
  extends:
    - .ostree-commit
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-amd64-fixedfunction

ostree-commit-armhf-fixedfunction-uboot:
  extends:
    - .ostree-commit
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-armhf-fixedfunction

ostree-commit-arm64-fixedfunction-uboot:
  extends:
    - .ostree-commit
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

ostree-commit-amd64-hmi-uefi:
  extends:
    - .ostree-commit
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-amd64-hmi

ostree-commit-armhf-hmi-uboot:
  extends:
    - .ostree-commit
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-armhf-hmi

ostree-commit-arm64-hmi-uboot:
  extends:
    - .ostree-commit
    - .arm64-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-arm64-hmi

######
# stage: artifacts build

sysroot-build-amd64-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .amd64-uefi
    - .sysroot
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-amd64-sysroot

sysroot-build-armhf-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .armhf-uboot
    - .sysroot
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-armhf-sysroot

sysroot-build-arm64-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .arm64-uboot
    - .sysroot
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-arm64-sysroot

ostree-rollbackbundle-build-amd64-fixedfunction-uefi:
  extends:
    - .ostree-rollbackbundle-build
    - .amd64-uefi
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-amd64-fixedfunction-uefi

ostree-rollbackbundle-build-armhf-fixedfunction-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .armhf-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-armhf-fixedfunction-uboot

ostree-rollbackbundle-build-arm64-fixedfunction-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .arm64-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-arm64-fixedfunction-uboot

ostree-rollbackbundle-build-amd64-hmi-uefi:
  extends:
    - .ostree-rollbackbundle-build
    - .amd64-uefi
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-amd64-hmi-uefi

ostree-rollbackbundle-build-armhf-hmi-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .armhf-uboot
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-armhf-hmi-uboot

ostree-rollbackbundle-build-arm64-hmi-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .arm64-uboot
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-arm64-hmi-uboot

ostree-bundle-build-amd64-fixedfunction-uefi:
  extends:
    - .ostree-bundle-build
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-amd64-fixedfunction-uefi

ostree-bundle-build-armhf-fixedfunction-uboot:
  extends:
    - .ostree-bundle-build
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-armhf-fixedfunction-uboot

ostree-bundle-build-arm64-fixedfunction-uboot:
  extends:
    - .ostree-bundle-build
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-arm64-fixedfunction-uboot

ostree-bundle-build-amd64-hmi-uefi:
  extends:
    - .ostree-bundle-build
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-amd64-hmi-uefi

ostree-bundle-build-armhf-hmi-uboot:
  extends:
    - .ostree-bundle-build
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-armhf-hmi-uboot

ostree-bundle-build-arm64-hmi-uboot:
  extends:
    - .ostree-bundle-build
    - .arm64-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-arm64-hmi-uboot

ostree-image-build-amd64-fixedfunction-uefi:
  extends:
    - .ostree-image-build
    - .amd64-uefi
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-amd64-fixedfunction-uefi

ostree-image-build-armhf-fixedfunction-uboot:
  extends:
    - .ostree-image-build
    - .armhf-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-armhf-fixedfunction-uboot

ostree-image-build-arm64-fixedfunction-uboot:
  extends:
    - .ostree-image-build
    - .arm64-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-arm64-fixedfunction-uboot

ostree-image-build-arm64-fixedfunction-rpi64:
  extends:
    - .ostree-image-build
    - .arm64-rpi64
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-arm64-fixedfunction-uboot
  variables:
    repo: repo-${architecture}-uboot-${type}/
    branch: ${osname}/${release}/${architecture}-uboot/${type}

ostree-image-build-amd64-hmi-uefi:
  extends:
    - .ostree-image-build
    - .amd64-uefi
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-amd64-hmi-uefi

ostree-image-build-armhf-hmi-uboot:
  extends:
    - .ostree-image-build
    - .armhf-uboot
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-armhf-hmi-uboot

ostree-image-build-arm64-hmi-rpi64:
  extends:
    - .ostree-image-build
    - .arm64-rpi64
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ostree-commit-arm64-hmi-uboot
  variables:
    repo: repo-${architecture}-uboot-${type}/
    branch: ${osname}/${release}/${architecture}-uboot/${type}

apt-image-build-amd64-fixedfunction-uefi:
  extends:
    - .apt-image-build
    - .amd64-uefi
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-amd64-fixedfunction

apt-image-build-armhf-fixedfunction-uboot:
  extends:
    - .apt-image-build
    - .armhf-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-armhf-fixedfunction

apt-image-build-arm64-fixedfunction-uboot:
  extends:
    - .apt-image-build
    - .arm64-uboot
    - .fixedfunction
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

apt-image-build-arm64-fixedfunction-rpi64:
  extends:
    - .apt-image-build
    - .arm64-rpi64
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

apt-image-build-arm64-fixedfunction-roc-pc-rk3399:
  extends:
    - .apt-image-build
    - .arm64-rk3399
    - .arm64-roc-pc-rk3399
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

apt-image-build-arm64-fixedfunction-rock-pi-4-rk3399:
  extends:
    - .apt-image-build
    - .arm64-rk3399
    - .arm64-rock-pi-4-rk3399
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

apt-image-build-arm64-fixedfunction-am62x:
  extends:
    - .apt-image-build
    - .arm64-am62x
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction

apt-image-build-amd64-hmi-uefi:
  extends:
    - .apt-image-build
    - .amd64-uefi
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-amd64-hmi

apt-image-build-armhf-hmi-uboot:
  extends:
    - .apt-image-build
    - .armhf-uboot
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-armhf-hmi

apt-image-build-arm64-hmi-uboot:
  extends:
    - .apt-image-build
    - .arm64-uboot
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-arm64-hmi

apt-image-build-arm64-hmi-rpi64:
  extends:
    - .apt-image-build
    - .arm64-rpi64
    - .hmi
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-arm64-hmi

apt-image-build-amd64-basesdk-sdk:
  extends:
    - .apt-image-build
    - .amd64-sdk
    - .basesdk
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-amd64-basesdk
    - ospack-build-armhf-devroot

apt-image-build-amd64-sdk-sdk:
  extends:
    - .apt-image-build
    - .amd64-sdk
    - .sdk
    - .try-immediate-upload-artifacts
  needs:
    - prepare-build-env
    - ospack-build-amd64-sdk
    - ospack-build-armhf-devroot

installer-image-build-mx6qsabrelite-uboot:
  extends:
    - .installer-image-build
  variables:
    target: 'mx6qsabrelite-uboot'

installer-image-build-imx8mn-var-som-uboot:
  extends:
    - .installer-image-build
  variables:
    target: 'imx8mn-var-som-uboot'

oslist-build-arm64-fixedfunction-rpi64:
  extends:
    - .lightweight
    - .arm64-rpi64
    - .fixedfunction
    - .oslist-generate
  needs:
    - prepare-build-env
    - ostree-image-build-arm64-fixedfunction-rpi64
    - apt-image-build-arm64-fixedfunction-rpi64

oslist-build-arm64-hmi-rpi64:
  extends:
    - .lightweight
    - .arm64-rpi64
    - .hmi
    - .oslist-generate
  needs:
    - prepare-build-env
    - ostree-image-build-arm64-hmi-rpi64
    - apt-image-build-arm64-hmi-rpi64

######
# stage: upload

artifacts-upload-amd64-fixedfunction-uefi:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-amd64-fixedfunction
    - ostree-bundle-build-amd64-fixedfunction-uefi
    - ostree-rollbackbundle-build-amd64-fixedfunction-uefi
    - ostree-image-build-amd64-fixedfunction-uefi
    - apt-image-build-amd64-fixedfunction-uefi

artifacts-upload-armhf-fixedfunction-uboot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-armhf-fixedfunction
    - ostree-bundle-build-armhf-fixedfunction-uboot
    - ostree-rollbackbundle-build-armhf-fixedfunction-uboot
    - ostree-image-build-armhf-fixedfunction-uboot
    - apt-image-build-armhf-fixedfunction-uboot

artifacts-upload-arm64-fixedfunction-uboot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ospack-build-arm64-fixedfunction
    - ostree-bundle-build-arm64-fixedfunction-uboot
    - ostree-rollbackbundle-build-arm64-fixedfunction-uboot
    - ostree-image-build-arm64-fixedfunction-uboot
    - apt-image-build-arm64-fixedfunction-uboot
    - ostree-image-build-arm64-fixedfunction-rpi64
    - apt-image-build-arm64-fixedfunction-rpi64
    - apt-image-build-arm64-fixedfunction-roc-pc-rk3399
    - apt-image-build-arm64-fixedfunction-rock-pi-4-rk3399
    - apt-image-build-arm64-fixedfunction-am62x

artifacts-upload-amd64-hmi-uefi:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-amd64-hmi
    - ostree-bundle-build-amd64-hmi-uefi
    - ostree-rollbackbundle-build-amd64-hmi-uefi
    - ostree-image-build-amd64-hmi-uefi
    - apt-image-build-amd64-hmi-uefi

artifacts-upload-armhf-hmi-uboot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-armhf-hmi
    - ostree-bundle-build-armhf-hmi-uboot
    - ostree-rollbackbundle-build-armhf-hmi-uboot
    - ostree-image-build-armhf-hmi-uboot
    - apt-image-build-armhf-hmi-uboot

artifacts-upload-arm64-hmi-uboot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .arm64-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ospack-build-arm64-hmi
    - ostree-bundle-build-arm64-hmi-uboot
    - ostree-rollbackbundle-build-arm64-hmi-uboot
    - ostree-image-build-arm64-hmi-rpi64
    - apt-image-build-arm64-hmi-uboot
    - apt-image-build-arm64-hmi-rpi64

artifacts-upload-amd64-basesdk-sdk:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-sdk
    - .basesdk
  needs:
    - prepare-build-env
    - ospack-build-amd64-basesdk
    - apt-image-build-amd64-basesdk-sdk

artifacts-upload-amd64-sdk-sdk:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-sdk
    - .basesdk
  needs:
    - prepare-build-env
    - ospack-build-amd64-sdk
    - apt-image-build-amd64-sdk-sdk

artifacts-upload-amd64-sysroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-uefi
    - .sysroot
  needs:
    - prepare-build-env
    - ospack-build-amd64-sysroot
    - sysroot-build-amd64-sysroot

artifacts-upload-amd64-devroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .amd64-uefi
    - .devroot
  needs:
    - prepare-build-env
    - ospack-build-amd64-devroot

artifacts-upload-armhf-sysroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .armhf-uboot
    - .sysroot
  needs:
    - prepare-build-env
    - ospack-build-armhf-sysroot
    - sysroot-build-armhf-sysroot

artifacts-upload-armhf-devroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .armhf-uboot
    - .devroot
  needs:
    - prepare-build-env
    - ospack-build-armhf-devroot

artifacts-upload-arm64-sysroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .arm64-uboot
    - .sysroot
  needs:
    - prepare-build-env
    - ospack-build-arm64-sysroot
    - sysroot-build-arm64-sysroot

artifacts-upload-arm64-devroot:
  extends:
    - .lightweight
    - .artifacts-upload
    - .arm64-uboot
    - .devroot
  needs:
    - prepare-build-env
    - ospack-build-arm64-devroot

oslist-upload-arm64-rpi64:
  extends:
    - .lightweight
    - .arm64-rpi64
    - .artifacts-upload
    - .oslist-upload
  needs:
    - prepare-build-env
    - oslist-build-arm64-fixedfunction-rpi64
    - oslist-build-arm64-hmi-rpi64

ostree-push-amd64-fixedfunction-uefi:
  extends:
    - .lightweight
    - .ostree-push
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-amd64-fixedfunction-uefi

ostree-push-armhf-fixedfunction-uboot:
  extends:
    - .lightweight
    - .ostree-push
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-armhf-fixedfunction-uboot

ostree-push-arm64-fixedfunction-uboot:
  extends:
    - .lightweight
    - .ostree-push
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-commit-arm64-fixedfunction-uboot

ostree-push-amd64-hmi-uefi:
  extends:
    - .lightweight
    - .ostree-push
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-amd64-hmi-uefi

ostree-push-armhf-hmi-uboot:
  extends:
    - .lightweight
    - .ostree-push
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-armhf-hmi-uboot

ostree-push-arm64-hmi-uboot:
  extends:
    - .lightweight
    - .ostree-push
    - .arm64-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-commit-arm64-hmi-uboot

ostree-hawkbit-upload-amd64-fixedfunction-uefi:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-bundle-build-amd64-fixedfunction-uefi

ostree-hawkbit-upload-armhf-fixedfunction-uboot:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-bundle-build-armhf-fixedfunction-uboot

ostree-hawkbit-upload-arm64-fixedfunction-uboot:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - ostree-bundle-build-arm64-fixedfunction-uboot

ostree-hawkbit-upload-amd64-hmi-uefi:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - ostree-bundle-build-amd64-hmi-uefi

ostree-hawkbit-upload-armhf-hmi-uboot:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-bundle-build-armhf-hmi-uboot

ostree-hawkbit-upload-arm64-hmi-uboot:
  extends:
    - .lightweight
    - .ostree-hawkbit-upload
    - .arm64-uboot
    - .hmi
  needs:
    - prepare-build-env
    - ostree-bundle-build-arm64-hmi-uboot

sysroot-metadata-upload-amd64-sysroot:
  extends:
    - .lightweight
    - .sysroot-metadata-upload
    - .amd64-uefi
    - .sysroot
  needs:
    - prepare-build-env
    - sysroot-build-amd64-sysroot

sysroot-metadata-upload-armhf-sysroot:
  extends:
    - .lightweight
    - .sysroot-metadata-upload
    - .armhf-uboot
    - .sysroot
  needs:
    - prepare-build-env
    - sysroot-build-armhf-sysroot

sysroot-metadata-upload-arm64-sysroot:
  extends:
    - .lightweight
    - .sysroot-metadata-upload
    - .arm64-uboot
    - .sysroot
  needs:
    - prepare-build-env
    - sysroot-build-arm64-sysroot

installer-upload-mx6qsabrelite-uboot:
  extends:
    - .lightweight
    - .installer-upload
  # do not use `needs:` as that would start the job very early and make the
  # whole pipeline uninterruptible way too early to be useful
  dependencies:
    - prepare-build-env
    - installer-image-build-mx6qsabrelite-uboot

installer-upload-imx8mn-var-som-uboot:
  extends:
    - .lightweight
    - .installer-upload
  # do not use `needs:` as that would start the job very early and make the
  # whole pipeline uninterruptible way too early to be useful
  dependencies:
    - prepare-build-env
    - installer-image-build-imx8mn-var-som-uboot

######
# stage: generate tests
# stage: run tests

.generate-tests-default:
  extends: .generate-tests
  tags:
    - lightweight
  stage: generate tests
  variables:
    baseurl: $IMAGE_URL_PREFIX
    image_path: daily/$release
    image_buildid: $PIPELINE_VERSION
  before_script:
    - source_wip=$(case "${CI_COMMIT_BRANCH}" in $osname/*) echo false;; *) echo true;; esac)
    - |
      if [ "$SUBMIT_LAVA_JOBS" != 1 ]
      then
        echo "⏹️ Not actually submitting jobs to LAVA, set UPLOAD_ARTIFACTS=1 and SUBMIT_LAVA_JOBS=1 to force"
      fi
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$LAVA_URL == null'
      when: never
    - if: '$LAVA_TOKEN == null'
      when: never
    - if: '$CI_COMMIT_BRANCH'
      when: on_success

.run-tests-default:
  extends: .run-tests
  stage: run tests
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$LAVA_URL == null'
      when: never
    - if: '$LAVA_TOKEN == null'
      when: never
    - if: '$SUBMIT_LAVA_JOBS == "1"' # only works for externally-set SUBMIT_LAVA_JOBS, variables set in build-env can't be seen here
      when: on_success
    - if: *osname_branch_rule # ideally $SUBMIT_LAVA_JOBS = 1 would be enough, but when it is computed in build-env it can't control jobs
      when: on_success

.generate-tests-apt:
  extends: .generate-tests-default
  variables:
    image_name: ${osname}_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    deployment: apt

.generate-tests-ostree:
  extends: .generate-tests-default
  variables:
    image_name: ${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    image_bundle: ${image_name}
    deployment: ostree

generate-tests-apt-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-fixedfunction-uefi
      artifacts: false

run-tests-apt-amd64-fixedfunction-uefi:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-amd64-fixedfunction-uefi
  trigger:
    include:
      - job: generate-tests-apt-amd64-fixedfunction-uefi
        artifact: child-pipeline.yaml

generate-tests-apt-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-armhf-fixedfunction-uboot
      artifacts: false

run-tests-apt-armhf-fixedfunction-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-armhf-fixedfunction-uboot
  trigger:
    include:
      - job: generate-tests-apt-armhf-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-fixedfunction-uboot
      artifacts: false

run-tests-apt-arm64-fixedfunction-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-arm64-fixedfunction-uboot
  trigger:
    include:
      - job: generate-tests-apt-arm64-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-hmi-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-hmi-uefi
      artifacts: false

run-tests-apt-amd64-hmi-uefi:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-amd64-hmi-uefi
  trigger:
    include:
      - job: generate-tests-apt-amd64-hmi-uefi
        artifact: child-pipeline.yaml

generate-tests-apt-armhf-hmi-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - job: artifacts-upload-armhf-hmi-uboot
      artifacts: false

run-tests-apt-armhf-hmi-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-armhf-hmi-uboot
  trigger:
    include:
      - job: generate-tests-apt-armhf-hmi-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-basesdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .basesdk
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-basesdk-sdk
      artifacts: false

run-tests-apt-amd64-basesdk-sdk:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-amd64-basesdk-sdk
  trigger:
    include:
      - job: generate-tests-apt-amd64-basesdk-sdk
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-sdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .sdk
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-sdk-sdk
      artifacts: false

run-tests-apt-amd64-sdk-sdk:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-amd64-sdk-sdk
  trigger:
    include:
      - job: generate-tests-apt-amd64-sdk-sdk
        artifact: child-pipeline.yaml

generate-tests-ostree-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-fixedfunction-uefi
      artifacts: false
    - job: ostree-push-amd64-fixedfunction-uefi
      artifacts: false

run-tests-ostree-amd64-fixedfunction-uefi:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-amd64-fixedfunction-uefi
  trigger:
    include:
      - job: generate-tests-ostree-amd64-fixedfunction-uefi
        artifact: child-pipeline.yaml

generate-tests-ostree-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .armhf-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-armhf-fixedfunction-uboot
      artifacts: false
    - job: ostree-push-armhf-fixedfunction-uboot
      artifacts: false

run-tests-ostree-armhf-fixedfunction-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-armhf-fixedfunction-uboot
  trigger:
    include:
      - job: generate-tests-ostree-armhf-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .arm64-uboot
    - .fixedfunction
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-fixedfunction-uboot
      artifacts: false
    - job: ostree-push-arm64-fixedfunction-uboot
      artifacts: false

run-tests-ostree-arm64-fixedfunction-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-arm64-fixedfunction-uboot
  trigger:
    include:
      - job: generate-tests-ostree-arm64-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-ostree-amd64-hmi-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .hmi
  needs:
    - prepare-build-env
    - job: artifacts-upload-amd64-hmi-uefi
      artifacts: false
    - job: ostree-push-amd64-hmi-uefi
      artifacts: false

run-tests-ostree-amd64-hmi-uefi:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-amd64-hmi-uefi
  trigger:
    include:
      - job: generate-tests-ostree-amd64-hmi-uefi
        artifact: child-pipeline.yaml

generate-tests-ostree-armhf-hmi-uboot:
  extends:
    - .generate-tests-ostree
    - .armhf-uboot
    - .hmi
  needs:
    - prepare-build-env
    - job: artifacts-upload-armhf-hmi-uboot
      artifacts: false
    - job: ostree-push-armhf-hmi-uboot
      artifacts: false

run-tests-ostree-armhf-hmi-uboot:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-armhf-hmi-uboot
  trigger:
    include:
      - job: generate-tests-ostree-armhf-hmi-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-fixedfunction-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .fixedfunction
  variables:
    profile_name: ${osname}-${type}-${architecture}-${board}-rpi4
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-fixedfunction-uboot
      artifacts: false

run-tests-apt-arm64-fixedfunction-rpi4:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-arm64-fixedfunction-rpi4
  trigger:
    include:
      - job: generate-tests-apt-arm64-fixedfunction-rpi4
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-fixedfunction-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .fixedfunction
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot_${PIPELINE_VERSION}
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-fixedfunction-uboot
      artifacts: false

run-tests-ostree-arm64-fixedfunction-rpi4:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-arm64-fixedfunction-rpi4
  trigger:
    include:
      - job: generate-tests-ostree-arm64-fixedfunction-rpi4
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-hmi-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .hmi
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-hmi-uboot
      artifacts: false

run-tests-apt-arm64-hmi-rpi4:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-apt-arm64-hmi-rpi4
  trigger:
    include:
      - job: generate-tests-apt-arm64-hmi-rpi4
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-hmi-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .hmi
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot_${PIPELINE_VERSION}
  needs:
    - prepare-build-env
    - job: artifacts-upload-arm64-hmi-uboot
      artifacts: false

run-tests-ostree-arm64-hmi-rpi4:
  extends:
    - .run-tests-default
  needs:
    - generate-tests-ostree-arm64-hmi-rpi4
  trigger:
    include:
      - job: generate-tests-ostree-arm64-hmi-rpi4
        artifact: child-pipeline.yaml

firmware-arm64-rcar-gen3-uboot:
  stage: firmware extraction
  extends:
    - .lightweight
  variables:
    target: rcar-gen3-uboot
    WORKSPACE: $CI_PROJECT_DIR
  script:
    - mkdir a/${PIPELINE_VERSION}/firmware
    - cd a/${PIPELINE_VERSION}/firmware
    - $CI_PROJECT_DIR/scripts/${target}-extraction.sh "${release}" "${mirror}" "${osname}"
  artifacts:
    expire_in: 1d
    paths:
      - a/*/firmware/*/*/*
  needs:
    - prepare-build-env

.firmware-imx8mn-bsh-smm-s2-uboot-base:
  stage: firmware extraction
  extends:
    - .lightweight
  variables:
    WORKSPACE: $CI_PROJECT_DIR
  script:
    - mkdir a/${PIPELINE_VERSION}/firmware
    - cd a/${PIPELINE_VERSION}/firmware
    - $CI_PROJECT_DIR/scripts/imx8mn_bsh_smm_s2_uboot.sh "${architecture}" "${mirror}" "${osname}" "${release}" "${target}"
  artifacts:
    expire_in: 1d
    paths:
      - a/*/firmware/*/*/*
  needs:
    - prepare-build-env

firmware-imx8mn-bsh-smm-s2-uboot:
  extends:
    - .firmware-imx8mn-bsh-smm-s2-uboot-base
  variables:
    target: imx8mn_bsh_smm_s2

firmware-imx8mn-bsh-smm-s2pro-uboot:
  extends:
    - .firmware-imx8mn-bsh-smm-s2-uboot-base
  variables:
    target: imx8mn_bsh_smm_s2pro

firmware-upload:
  stage: firmware upload
  extends:
    - .lightweight
    - .artifacts-upload
  # do not use `needs:` as that would start the job very early and make the
  # whole pipeline uninterruptible way too early to be useful
  dependencies:
    - prepare-build-env
    - firmware-arm64-rcar-gen3-uboot
    - firmware-imx8mn-bsh-smm-s2-uboot
    - firmware-imx8mn-bsh-smm-s2pro-uboot
